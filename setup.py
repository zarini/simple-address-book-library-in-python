from setuptools import setup

setup(
    name='addressbook',
    version='1',
    description='A package as assignment to implement a simple address book library in Python',
    url='https://bitbucket.org/zarini/simple-address-book-library-in-python',
    author='Mohammad Nuairat',
    author_email='mhn.zarini@gmail.com',
    license='MIT',
    packages=['addressbook'],
    test_suite='nose.collector',
    tests_require=['nose'],
    zip_safe=False,
)
