Address Book Assignment
================================
The assignment is to implement a simple address book library in Python.
check requirements[here](https://github.com/gingerpayments/hiring/blob/master/coding-assignments/python-address-book-assignment/python-address-book-assignment.rst#requirements)

Usage
================================

*Requirements*

- Python 2.7

*Installation*

```
mkdir addressbook
cd addressbook
virtualenv env
source env/bin/activate

git clone https://bitbucket.org/zarini/simple-address-book-library-in-python.git
cd addressbook
python setup.py test
pip install .
cd ..
python test_addressbook.py
```


*API call examples.*

``` python

from addressbook import AddressBook

address_book = AddressBook()

person_dict = {
    'first_name': 'John',
    'last_name': 'Smith',
    'emails': ['johns@example.com'],
    'phone_numbers': ['555-555-1234'],
    'street_addresses': ['American Consul General, Department of State, 5520 Quebec Place, Washington, US'],
    'groups': ['favorites'],
}

* Add a person to the address book.
person_obj = address_book.add_person(**person_dict)

* Given a person we want to easily find the groups the person belongs to.
person_obj.get_groups()

* Add a group to the address book.
address_book.add_group('friends')

* Given a group we want to easily find its members.
address_book.find_members('favorites').members

* Find person by name.
result = self.address_book.find_persons_by_name(first_name='John')

* Find person by email address.
result = self.address_book.find_persons_by_email(email='johns@example.com')

```

*Design-only questions*

>Find person by email address (can supply any substring, ie. "comp" should work assuming "alexander@company.com" is an email address in the address book) - discuss how you would implement this without coding the solution.

I would index all persons emails into a hash(faster for search), or tree them by company (after the @ part of the email). then check `if 'comp' in "alexander@company.com"` while looking for relevant objects.