

class Person(object):
    """A person.
    """
    def __init__(self, first_name, last_name, street_addresses=[], emails=[], phone_numbers=[]):
        """ Return a Person object whose name is *first_name last_name*
            one or more street addresses.
            one or more street addresses.
            one or more email addresses.
            one or more phone numbers.
        """
        self.first_name = first_name
        self.last_name = last_name
        self.street_addresses = street_addresses
        self.emails = emails
        self.phone_numbers = phone_numbers
        self.groups = []

    def add_email(self, *email):
        self.emails.extend(email)

    def add_street_address(self, *street_address):
        self.street_addresses.extend(street_address)

    def add_phone_number(self, *phone_number):
        self.phone_numbers.extend(phone_number)

    def add_group(self, group):
        self.groups.append(group)

    def get_groups(self):
        return [value.name for value in self.groups]

    def get_full_name(self):
        return "%s %s" % (self.first_name, self.last_name)


class Group(object):
    """
        person can be a member of one or more groups.
    """
    def __init__(self, name):
        self.name = name
        self.members = []

    def add_member(self, member):
        self.members.extend(member)

    def get_member(self, member):
        return self.members


class AddressBook(object):
    """
    Class AddressBook with states and behaviors
    Assumptions:
        An address book is a collection of persons and groups.
    """

    groups = {}
    persons = {}

    def add_person(self, first_name, last_name, street_addresses=[], emails=[], phone_numbers=[], groups=[]):
        """
        Add a person to the address book.
        :param first_name: person first name
        :param last_name: person last name
        :param street_addresses: list of all person known addresses
        :param emails: list of user emails
        :param phone_numbers: list of user phone numbers
        :param groups: list if all groups this person belong to
        :return: created person instance
        """
        new_person = Person(first_name, last_name, street_addresses, emails, phone_numbers)
        for group_name in groups:
            if group_name in self.groups.keys():
                group = self.groups[group_name]
            else:
                group = self.add_group(group_name)
            group.add_member([new_person])
            new_person.add_group(group)
        self.persons[new_person.get_full_name()] = new_person
        return new_person

    def add_group(self, group_name):
        """
            Add a group to the address book.
        :param group_name: the name of group to be created
        :return: None
        """
        if group_name in self.groups.keys():
            return self.groups[group_name]
        new_group = Group(group_name)
        self.groups[group_name] = new_group
        return new_group

    def find_members(self, group_name):
        """
            Given a group we want to find its members
        :param group_name: the name of group to search in
        :return: list of persons
        """
        if group_name not in self.groups.keys():
            raise KeyError(
                '%s group was not found' % group_name
            )
        return self.groups[group_name].members

    def find_persons_by_name(self, first_name=None, last_name=None):
        """
        Find person by name (can supply either first name, last name, or both).
        :param first_name: person first name
        :param last_name: person last name
        :return: list of persons
        """
        search_key = ""
        if first_name and last_name:
            search_key = first_name + ' ' + last_name
        elif first_name:
            search_key = first_name
        elif last_name:
            search_key = last_name

        return [value for key, value in self.persons.items() if search_key.lower() in key.lower()]

    def find_persons_by_email(self, email):
        """
        Find person by email address (can supply either the exact string or a prefix string, ie. both
        "alexander@company.com" and "alex" should work).
        :param email: email exact string or a prefix string
        :return: list of persons
        """
        return [value for key, value in self.persons.items() if bool(filter(lambda x: x.startswith(email), value.emails))]
