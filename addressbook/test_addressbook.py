import unittest

from addressbook import AddressBook, Person


class TestAddressBookApiMethods(unittest.TestCase):

    def setUp(self):
        self.address_book = AddressBook()

    def test_add_person(self):

        person_dict = {
            'first_name': 'John',
            'last_name': 'Smith',
            'emails': ['johns@example.com'],
            'phone_numbers': ['555-555-1234'],
            'street_addresses': ['American Consul General, Department of State, 5520 Quebec Place, Washington, US'],
            'groups': ['favorites'],
        }

        result = self.address_book.add_person(**person_dict)

        self.assertIsInstance(result, Person)
        self.assertIn(result, self.address_book.persons.values())

        self.assertEqual(result.first_name, person_dict['first_name'])
        self.assertEqual(result.last_name, person_dict['last_name'])

        self.assertListEqual(result.emails, person_dict['emails'])
        self.assertListEqual(result.phone_numbers, person_dict['phone_numbers'])
        self.assertListEqual(result.street_addresses, person_dict['street_addresses'])

        self.assertListEqual(result.get_groups(), person_dict['groups'])

    def test_add_group(self):

        result_group = self.address_book.add_group('friends')

        self.assertIn(result_group, self.address_book.groups.values())

    def test_find_members(self):

        leanne_dict = {
            'first_name': 'Leanne',
            'last_name': 'Graham',
            'emails': ['CEO@thecompany.com', 'LeanneG@otherplace.gif'],
            'phone_numbers': ['999-999-9999'],
            'street_addresses': ['Anthony Benoit 490 E, Main Street Norwich, CT 06360, US'],
            'groups': ['favorites', 'friends'],
        }

        leanne_person = self.address_book.add_person(**leanne_dict)

        yuto_dict = {
            'first_name': 'Yuto',
            'last_name': 'Murase',
            'emails': ['yuto@example.com'],
            'phone_numbers': ['555-555-4321'],
            'street_addresses': ['42-1 Motohakone Hakonemaci, Ashigarashimo-Gun, Kanagawa 250-05, Japan'],
            'groups': ['favorites'],
        }

        yuto_person = self.address_book.add_person(**yuto_dict)

        result = self.address_book.find_members('favorites')

        self.assertIn(yuto_person, result)
        self.assertIn(leanne_person, result)

        result = self.address_book.find_members('friends')

        self.assertIn(leanne_person, result)
        self.assertNotIn(yuto_person, result)

        with self.assertRaises(KeyError):
            result = self.address_book.find_members('my_friends')

    def test_find_groups(self):
        thomas_dict = {
            'first_name': 'Thomas',
            'last_name': 'Kaszas',
            'emails': ['thomask@example.com'],
            'phone_numbers': ['555-555-7777'],
            'street_addresses': ['5322 Otter Lane, Middleberge FL 32068'],
            'groups': ['favorites', 'friends'],
        }

        thomas_person = self.address_book.add_person(**thomas_dict)

        result1 = self.address_book.find_persons_by_name(first_name='Thomas')
        self.assertEqual(result1[0].get_groups(), thomas_dict['groups'])
        self.assertNotIn('people', result1[0].get_groups())

    def test_find_persons_by_name(self):
        chang_dict = {
            'first_name': 'Cheng',
            'last_name': 'Li',
            'emails': ['cli@company.com'],
            'phone_numbers': ['555-555-0000'],
            'street_addresses': ['7-301 Houjie, Middle of JiangNan Road, Guang Zhou 510240, GuangDong  P.R. China'],
            'groups': ['people'],
        }

        chang_person = self.address_book.add_person(**chang_dict)

        result1 = self.address_book.find_persons_by_name(first_name='Cheng')
        self.assertEqual(result1, [chang_person])

        result2 = self.address_book.find_persons_by_name(first_name='Cheng', last_name='Li')
        self.assertEqual(result2, [chang_person])

        result3 = self.address_book.find_persons_by_name(last_name='Li')
        self.assertEqual(result3, [chang_person])

        result4 = self.address_book.find_persons_by_name(last_name='L')
        self.assertIn(chang_person, result4)

        result5 = self.address_book.find_persons_by_name(last_name='Leanne')
        self.assertNotIn(chang_person, result5)

    def test_find_persons_by_email(self):

        bader_dict = {
            'first_name': 'Bader',
            'last_name': 'Alkhalefa',
            'emails': ['bader@company.com'],
            'phone_numbers': ['555-555-1111'],
            'street_addresses': ['Aramco P.O. Box 9239, Dhahran 31311, Saudi Arabia'],
            'groups': ['people'],
        }
        bader_person = self.address_book.add_person(**bader_dict)

        result1 = self.address_book.find_persons_by_email(email='bader@company.com')
        self.assertEqual(result1, [bader_person])

        result2 = self.address_book.find_persons_by_email(email='bader')
        self.assertEqual(result2, [bader_person])


if __name__ == '__main__':
    unittest.main()
